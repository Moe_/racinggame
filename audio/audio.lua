function load_audio()
    local audioPath = "res/sfx/"
    gMusic = love.audio.newSource(audioPath.."background.ogg")

    gExplosion = love.audio.newSource(audioPath.."explosion.wav", "static")
    gCrash = love.audio.newSource(audioPath.."crash.wav", "static")
    gPluslap = love.audio.newSource(audioPath.."pluslap.wav", "static")
    gMinuslap = love.audio.newSource(audioPath.."minuslap.wav", "static")
end

function start_music()
    gMusic:setLooping(true)
    gMusic:play()
end

function audio_play_explosion()
    gExplosion:play()
end

function audio_play_crash()
    gCrash:play()
end

function audio_play_pluslap()
    gPluslap:play()
end

function audio_play_minuslap()
    gMinuslap:play()
end