require "ship"

class "World"{}

function World:__init()
    self.loveWorld = love.physics.newWorld(0, 0, true)
    self.loveWorld:setCallbacks(beginContact,endContact,preSolve,postSolve)
end

function World:update(dt)
    self.loveWorld:update(dt)
end