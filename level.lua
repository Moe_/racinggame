class "Level" {
    filename = {};
    playerPosition = {};
    asteroids = {};
    start = {0, 0, 0, 0};
}
function Level:__init(name)
    self.scaleFactor = 32
    self.filename = name
    self.imageData = love.image.newImageData(self.filename)
    self.image = love.graphics.newImage(self.imageData)
    self:createStructures()
    self.numborders = 0
    self.borderVec = {}
end

function Level:draw(offset)
    love.graphics.setColor(255,255,255)
    love.graphics.draw(self.image, 0 + offset[1], 0 + offset[2], 0, self.scaleFactor, self.scaleFactor)
end

function Level:update(dt)

end

function Level:get(x, y)
    if (x>511 or y > 511 or x<0 or y<0)then
        return 0
            --        print (x,y)
    end
    local r = self:getRaw(x, y)
    if r > 128 then
        return 1
    else
        return 0
    end
end

function Level:getRaw(x, y)
    local r, g, b, a = self.imageData:getPixel(x, y)
    return r
end

function Level:createStructures()
    local w, h = self.imageData:getWidth(), self.imageData:getHeight()
    for x = 0, w - 1, 1 do
        for y = 0, h - 1, 1 do
            local id = self:getRaw(x, y)
            -- add object creation
            if id == 1 then
            --print("Start!")
            elseif id == 0 then
                local size = self:checkBox(x, y)
            elseif id == 2 then
                table.insert(self.asteroids, {x, y})
            end
        end
    end
    self:detectStart(w/2, h/2, w/2)
    print ("Level: ", "Asteroids: " .. #self.asteroids, "Start: " .. self.playerPosition[1] .. ", " .. self.playerPosition[2])
end

function Level:detectStart(x, y, range)
    local startTrack = 0
    local endTrack = 0
    local lastId = 0
    for i = x, x + range - 1, 1 do
        local id = self:get(i, y)
        if id == 1 and lastId == 0 then
            startTrack = i
        end
        if id == 0 and lastId == 1 then
            endTrack = i
        end
        lastId = id
    end
    self.start[1] = self.scaleFactor * startTrack
    self.start[3] = self.scaleFactor * endTrack
    self.start[2] = self.scaleFactor * y
    self.start[4] = self.scaleFactor * y
    self.playerPosition[1] = self.scaleFactor * ((startTrack + endTrack) / 2)
    self.playerPosition[2] = self.scaleFactor * y
end

function Level:detectWalls(x, y)
    local w, h = self.imageData:getWidth(), self.imageData:getHeight()
    x = x / self.scaleFactor
    y = y / self.scaleFactor
    local res = {-1, -1, -1, -1}
    local lastId = 0
    -- left
    for i = x, 0, -1 do
        local id = self:get(i, y)
        if id == 0 and lastId == 1 then
            res[1] = i
            break
        end
        lastId = id
    end
    -- right
    for i = x, w - 1, 1 do
        local id = self:get(i, y)
        if id == 0 and lastId == 1 then
            res[2] = i
            break
        end
        lastId = id
    end
    -- top
    for i = y, 0, -1 do
        local id = self:get(x, i)
        if id == 0 and lastId == 1 then
            res[3] = i
            break
        end
        lastId = id
    end
    -- bottom
    for i = y, h - 1, 1 do
        local id = self:get(x, i)
        if id == 0 and lastId == 1 then
            res[4] = i
            break
        end
        lastId = id
    end
    return res
end

function Level:checkBox(x, y)
-- TODO if we want to have large objects
end

function Level:getPlayerPosition()
    return self.playerPosition
end

function Level:createMap()
    local w, h = self.imageData:getWidth(), self.imageData:getHeight()
    for x = 0, w-2, 1 do
        for y = 0, h-2, 1 do
            local tl = 1 - self:get(x,y)
            local tr = 1 - self:get(x+1,y)
            local bl = 1 - self:get(x,y+1)
            local br = 1 - self:get(x+1,y+1)
            self:makePhysTile(tl,tr,bl,br,x*self.scaleFactor,y*self.scaleFactor)
        end
    end
end

function Level:makePhysTile(tl,tr,bl,br,x,y)
    local sum = tl + tr + bl + br
    local verts = {}
    local H = self.scaleFactor/2
    -- If sum == 0 or sum == 4, we make no physics object
    if sum == 1 then
        -- This is a small single corner object, assuming filled is top left
        verts = {0,-1*H,
            -1*H,-1*H,
            -1*H,0}
        if tr == 1 then verts = rotate(verts,1)
        elseif br == 1 then verts = rotate(verts,2)
        elseif bl == 1 then verts = rotate(verts,3) end
        self:createBarrier(verts,{x+H,y+H})
    end
    if sum == 2 then
        -- This is a flat block OR two opposite corner object
        if (bl + tr)%2 == 1 then
            -- Flat surface, assume both solid squares on the left
            verts = {0,-1*H,
                -1*H,-1*H,
                -1*H,H,
                0,H}
            if (bl + br) == 2 then verts = rotate(verts,3)
            elseif (br + tr) == 2 then verts = rotate(verts,2)
            elseif (tr + tl) == 2 then verts = rotate(verts,1) end
            self:createBarrier(verts,{x+H,y+H})
        else
            -- Double corner object, assume top left and bottom right solid
            verts = {0,-1*H,
                -1*H,-1*H,
                -1*H,0}
            if tl == 0 then verts = rotate(verts,1) end
            self:createBarrier(verts,{x+H,y+H})
            -- Make the first object
            verts = {H,0,
                H,H,
                0,H}
            if tl == 0 then verts = rotate(verts,1) end
            self:createBarrier(verts,{x+H,y+H})
        end
    end
    if sum == 3 then
        -- This is a small single corner hole, assuming empty is top left
        verts = {0,-1*H,
            -1*H,0,
            -1*H,H,
            H,H,
            H,-1*H}
        if bl == 0 then verts = rotate(verts,3)
        elseif br == 0 then verts = rotate(verts,2)
        elseif tr == 0 then verts = rotate(verts,1) end
        self:createBarrier(verts,{x+H,y+H})
    end
end

function rotate(pList,rot)
    -- Perform "rot" clockwise 90 degree rotations of the following points about the origin
    local c1,c2 = 0,0
    for x = 1, #pList, 2 do
        c1,c2 = pList[x],pList[x+1]
        if rot == 1 then
            pList[x] = -1*c2
            pList[x+1] = c1
        end
        if rot == 2 then
            pList[x] = -1*c1
            pList[x+1] = -1*c2
        end
        if rot == 3 then
            pList[x] = c2
            pList[x+1] = -1*c1
        end
    end
    return pList
end

function Level:createBarrier(pList,loc)
    self.numborders = self.numborders + 1
    local b = love.physics.newBody(world.loveWorld,loc[1],loc[2],"static")
    local s = love.physics.newPolygonShape(unpack(pList))
    local f = love.physics.newFixture(b,s)
    f:setUserData("Barrier" .. self.numborders)
    local all = {}
    all.b = b
    all.s = s
    all.f = f
    self.borderVec[self.numborders] = all
end

function Level:drawBarrier(o)
    local points = {}
    local cnt = 0
    for i = 1, self.numborders do
        cnt = 1 - cnt
        love.graphics.setColor(191 * cnt + 63, 191 * cnt + 63, 63 * cnt - 63)
        points = {self.borderVec[i].b:getWorldPoints(self.borderVec[i].s:getPoints())}
        translate(points, o)
        love.graphics.polygon("fill", points)
    end
end

function translate(vlist,trans)
    for x=1, #vlist, 2 do
        vlist[x] = vlist[x] + trans[1]
        vlist[x+1] = vlist[x+1] + trans[2]
    end
end

function Level:getStartPosition()
    return self.start
end
