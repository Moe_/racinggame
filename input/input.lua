class "Input"{}
function Input:__init()
    self.cameras = {"1","2"}
end

function Input:setCameraCallback(cameraCallback)
    self.cameraCallback = cameraCallback
end

function Input:update()
end

function Input:checkForCameras(key)
    for i=1, #self.cameras do
        if key ==self.cameras[i] then
            self.cameraCallback(i)
            break
        end
    end

end

function love.keypressed(key)
    --check for camera change request
    input:checkForCameras(key)
    if key == " " then
        g.paused = not g.paused
    elseif key == "escape" then
        love.event.quit( )
	elseif key == "f1" then
		lightOn = not lightOn
		hud.light = lightOn
    end
end

function love.keyreleased(key)

end
