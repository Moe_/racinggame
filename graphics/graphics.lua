class "Graphics" {
}

function Graphics:__init()
    -- Image Font
    local font = love.graphics.newImageFont(GFX_PATH.."font.png",
        " abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789.,!?-+/():;%&`'*#=[]\"")
	font:setFilter("nearest", "nearest")
    love.graphics.setFont(font)

    --background of stars
    self.bg = love.graphics.newImage(GFX_PATH .. "bg.png")
	self.bg2 = love.graphics.newImage(GFX_PATH .. "bg2.png")
    self.bgQuad = love.graphics.newQuad(0, 0, love.graphics.getWidth() * 32, love.graphics.getHeight() * 32, 512, 512)
    self.bg:setWrap('repeat', 'repeat')
	self.bg2:setWrap('repeat', 'repeat')
end

function Graphics:draw()
	if lightOn then
		lightWorld.update()
	end

	love.postshader.setBuffer("render")
    love.graphics.setShader()
    love.graphics.setColor(255, 255, 255, 255)
    self.cameraPosition = g.shipVec[cameraShip]:getOffset() -- TODO: allow cameras independent of ships
    love.graphics.draw(self.bg, self.bgQuad, self.cameraPosition[1] / 4, self.cameraPosition[2] / 4)
	love.graphics.setColor(255, 191, 127, 31)
	love.graphics.draw(self.bg2, self.bgQuad, self.cameraPosition[1] / 2, self.cameraPosition[2] / 2)
	love.graphics.setColor(255, 255, 255)
    level:drawBarrier(self.cameraPosition)
    drawShips(self.cameraPosition)
    startGate:draw(self.cameraPosition)
	if lightOn then
		lightWorld.drawShadow()
	end
	asteroids:draw(self.cameraPosition)
    minimap:draw()
    drawDebug(self.cameraPosition)
    hud:draw(playerShip.shipEntity.health, playerShip.shipEntity.fuel)

    if (g.paused)then
        love.graphics.print("Press SPACE to unpause.",100,400,0,4,4)
    end

    love.postshader.draw("bloom")
end