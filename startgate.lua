class "StartGate" {
    lastPlayerPosY = {};
    lap = {};
}

function StartGate:__init(pos, playercount)
    self.pos = pos
    for i = 1, playercount, 1 do
        self.lastPlayerPosY[i] = pos[2] + 20
        self.lap[i] = 1
    end
end

function StartGate:update(dt, playerpos, player)
    if playerpos[1] > self.pos[1] and playerpos[1] < self.pos[3] then
        if playerpos[2] < self.pos[2] and self.lastPlayerPosY[player] >= self.pos[2] then
            self.lap[player] = self.lap[player] + 1
            if player == 1 then
                g.shipVec[1].health = g.startHealth
                g.shipVec[1].fuel = g.startFuel
                audio_play_pluslap()
            end
        elseif playerpos[2] > self.pos[2] and self.lastPlayerPosY[player] <= self.pos[2] then
            self.lap[player] = self.lap[player] - 1
            if player == 1 then
                audio_play_minuslap()
            end
        end
    end
    self.lastPlayerPosY[player] = playerpos[2]
end

function StartGate:draw(offset)
    love.graphics.setColor(255,255,0,128)
    love.graphics.rectangle("fill", self.pos[1] + offset[1], self.pos[2] + offset[2], self.pos[3] - self.pos[1], 20)
    love.graphics.setColor(255, 255, 255, 31)
    love.graphics.rectangle("fill", 0, 62, 128, 20)
    love.graphics.setColor(0, 127, 255,255)
    love.graphics.print("Lap: " .. self.lap[1], 16, 64)
    love.graphics.setColor(255, 255, 255, 255)
end