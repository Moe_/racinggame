class "ShipEntity" {}

function ShipEntity:__init(p, player)
    g.numplayers = g.numplayers + 1
    self.id = g.numplayers
    self.health = g.startHealth
    self.fuel = g.startFuel
    self.b = love.physics.newBody(world.loveWorld,p[1],p[2],"dynamic")
    self.s = love.physics.newCircleShape(32)
    self.f = love.physics.newFixture(self.b,self.s)
    self.b:setMass(1)
    self.b:setAngularDamping(5)
    self.f:setUserData("Player" .. self.id)
    self.f:setCategory(ShipCategory)
    self.f:setRestitution(0.1)
    self.old_vel = {0,0}
    self.lastTurn = 0
    self.lastPosition = {0, 0}
    self.timesNotMoved = 0
    self.accelerate = 0
    self.speed = 0

    self.player = player
    print "end of shipEntity init"

end

function shipCollideCheckStart(a, b, coll)
    -- Check to ensure that either a or b is the ship object
    -- and apply collision code if either of the objects is a ship
    if a:getCategory() == ShipCategory then
        shipCollideStart(a,coll)
    end
    if b:getCategory() == ShipCategory then
        shipCollideStart(b,coll)
    end
end

function shipCollideCheckEnd(a, b, coll)
    -- Check to ensure that either a or b is the ship object
    -- and apply collision code if either of the objects is a ship
    if a:getCategory() == ShipCategory then
        shipCollideEnd(a,coll)
    end
    if b:getCategory() == ShipCategory then
        shipCollideEnd(b,coll)
    end
end

function shipCollideStart(a,coll)
    getShipFromFixture(a).old_vel = {a:getBody():getLinearVelocity()}
    getShipFromFixture(a).crash_time = 1.0
end

function shipCollideEnd(a,coll)
    -- Damage the ship proportional to its speed squared
    local x,y = a:getBody():getLinearVelocity()
    local ox,oy = unpack(getShipFromFixture(a).old_vel)
    local speedDmg = ((x-ox)^2 + (y-oy)^2) * g.speedDmgMult
    getShipFromFixture(a).shipEntity.health = getShipFromFixture(a).shipEntity.health - speedDmg
    getShipFromFixture(a).old_vel = {x,y}
    if getShipFromFixture(a).shipEntity.health < 0 then
        audio_play_explosion()
    else
        audio_play_crash()
    end
    getShipFromFixture(a).crash:start()
end

function getShipFromFixture(a)
    -- Gets a ship from the fixture name
    local tstring = a:getUserData()
    return g.shipVec[tonumber(tstring:sub(7,#tstring))]
end


function ShipEntity:setPos(p)
    self.b:setX(p[1])
    self.b:setY(p[2])
end
function ShipEntity:getPos()
    return {self.b:getX(),self.b:getY()}
end
